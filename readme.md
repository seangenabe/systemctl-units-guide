# Basics

Start

```bash
systemctl start _unit_
```

Status

```bash
systemctl status _unit_
```

Stop

```bash
systemctl stop _unit_
```

## Create your own service

Create a new service

```bash
systemctl edit --full --force _name_.service
```

Edit the service

```bash
systemctl edit --full _name_.service
```

Write a unit file to describe the service

```
[Unit]
Description=

[Service]
Type=simple|exec|forking|oneshot
ExecStart=
ExecStop=
RemainAfterExit=

[Install]
WantedBy=_dependent_.target
```
